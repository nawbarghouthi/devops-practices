#! /bin/bash

#this script to update the mvn version with a specific format.

comm=$(git log  --graph --decorate --oneline | awk '{print $2; exit}')
date_test=$(git log --date=short | awk 'NR==3{print $2}')
date_fin=$(date -d $date_test +"%y%m%d")

version=$(echo "$comm-$date_fin")

find . -name "pom.xml" -exec mvn versions:set -DnewVersion="$version"-SNAPSHOT -f {} \; 
